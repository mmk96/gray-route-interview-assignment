package assignment.mayuri.interviewtest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class LocationListActivity extends AppCompatActivity implements  clickListenerCallBack {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private  int lastSelectedPostion=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_list);

        if(Utils.locationList.size()>0){
            TextView noItemTextView=findViewById(R.id.no_item_textview);
            noItemTextView.setVisibility(View.GONE);
            setUpRecyclerView();
        }

    }

    private void setUpRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new  LocationListAdapter(Utils.locationList,this);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(lastSelectedPostion!= -1) {

            RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(this) {
                @Override protected int getVerticalSnapPreference() {
                    return LinearSmoothScroller.SNAP_TO_START;
                }
            };
            smoothScroller.setTargetPosition(lastSelectedPostion);
            layoutManager.startSmoothScroll(smoothScroller);
        }


    }

    @Override
    public void clickListenerCallBack(int position) {
        Intent intent = new Intent(LocationListActivity.this, MainMapActivity.class);
        intent.putExtra(Utils.ARRAY_POSTION, position);
        lastSelectedPostion=position;
        startActivity(intent);
    }
}
