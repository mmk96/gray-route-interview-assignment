package assignment.mayuri.interviewtest;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;

public class Utils {

    public static ArrayList<locationListModel> locationList=new ArrayList<>();
public static final String ARRAY_POSTION="arrayPosition";


    public static boolean checkIfPermission(Context context, @NonNull String permission){
        if (ContextCompat.checkSelfPermission(context, permission)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }else {
            return true;
        }

    }
}
