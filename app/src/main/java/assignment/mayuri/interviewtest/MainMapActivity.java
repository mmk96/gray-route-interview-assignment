package assignment.mayuri.interviewtest;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainMapActivity extends FragmentActivity implements OnMapReadyCallback {
    private static final int REQUEST_IMAGE_CAPTURE = 101;
    private GoogleMap googleMap;
    private final int CAMERA_PERM_CODE = 100;
    private final int STORAGE_PERM_CODE = 102;
    private Bitmap mImageBitmap;
    private String mCurrentPhotoPath = null;
    private LatLng tempLatLong;
    private String tempAddress;

    Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_map);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
        setListenerForButton();


    }

    private void setListenerForButton() {
        button = findViewById(R.id.goto_list_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMapActivity.this, LocationListActivity.class));
            }
        });
    }

    private void clearTempDataAndSaveInDB() {

        Utils.locationList.add(new locationListModel(
                tempLatLong.latitude,
                tempLatLong.longitude,
                tempAddress != null ? tempAddress : "Unknown Address",
                mCurrentPhotoPath
        ));

        tempLatLong = null;
        mCurrentPhotoPath = null;
        tempAddress = null;
    }

    //region MAPS
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (googleMap != null) {


            if (getIntent().hasExtra(Utils.ARRAY_POSTION)) {
                setOneMarkerView();

            } else {
                addActionListenersOnMap();
            }
        }
    }

    private void setOneMarkerView() {
        button.setVisibility(View.GONE);

        final int pos = getIntent().getIntExtra(Utils.ARRAY_POSTION, 0);

        LatLng latLng = new LatLng(
                Utils.locationList.get(pos).getLat(),
                Utils.locationList.get(pos).getLog()
        );
        final Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng).title(getTitleForMarker(latLng)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 5));
        marker.setTag(pos);
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker markerClicked) {
                if (markerClicked.getTag() == marker.getTag()) {
                    Intent intent = new Intent(MainMapActivity.this, LocationListActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
                    Bundle bundle = new Bundle();
                    bundle.putInt(Utils.ARRAY_POSTION, pos);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
                return false;
            }
        });
    }

    private void addActionListenersOnMap() {
        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                tempLatLong = latLng;
                startCameraWork();
            }
        });


    }

    private String getTitleForMarker(LatLng latLng) {
        String titleMarker = "";
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            tempAddress = addresses.get(0).getAddressLine(0);

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null) {
            String city = addresses.get(0).getLocality();
            String country = addresses.get(0).getCountryName();
            if (city != null) {
                titleMarker = city + ", ";
            }
            if (country != null) {
                titleMarker += country;
            } else {
                return getString(R.string.unknown_location);
            }
            return titleMarker;
        } else {
            return getString(R.string.unknown_location);
        }
    }

    private void addMarker() {
        if (googleMap != null) {
            googleMap.addMarker(new MarkerOptions().position(tempLatLong).title(getTitleForMarker(tempLatLong)));
            clearTempDataAndSaveInDB();

        } else {
            Toast.makeText(this, "map error", Toast.LENGTH_LONG).show();

        }
    }
    //endregion

    //region CAMERA
    private void startCameraWork() {
        askPermissionForCamera();
    }

    private void askPermissionForCamera() {
        if (!Utils.checkIfPermission(getApplicationContext(), Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    CAMERA_PERM_CODE);
        } else {
            askPermissionForStorage();

        }
    }

    private void askPermissionForStorage() {
        // Create the File where the photo should go
        try {
            if (!Utils.checkIfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        CAMERA_PERM_CODE);
            } else {
                createImageFile();

            }
        } catch (Exception ex) {
            // Error occurred while creating the File
            Log.i("nfv", "IOException");
        }
        // Continue only if the File was successfully created
    }


    private void createImageFile() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,
                    ".jpg",
                    storageDir
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        if (image != null) {
            mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        }
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            if (image != null) {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            try {
                mImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(mCurrentPhotoPath));
                if (mImageBitmap != null) {
                    Toast.makeText(this, "image saved", Toast.LENGTH_LONG).show();
                    addMarker();
                }
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, "image IOException", Toast.LENGTH_LONG).show();

            }
        } else {
            Toast.makeText(this, "image error", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_PERM_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    askPermissionForStorage();
                } else {

                }
                return;
            }
            case STORAGE_PERM_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    createImageFile();
                } else {

                }
                return;
            }


        }

    }
    //endregion

}
