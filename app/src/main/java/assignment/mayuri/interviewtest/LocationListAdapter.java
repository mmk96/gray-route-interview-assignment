package assignment.mayuri.interviewtest;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.MyViewHolder> {
    private ArrayList<locationListModel> locationList;
    private assignment.mayuri.interviewtest.clickListenerCallBack clickListenerCallBack;

    public LocationListAdapter(ArrayList<locationListModel> locationList, clickListenerCallBack clickListenerCallBack) {
        this.locationList = locationList;
        this.clickListenerCallBack = clickListenerCallBack;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.location_list_item_layout, viewGroup, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {
        String textToDisplay = "Address : " + locationList.get(i).getAddress() + "\n" +
                "Latitude : " + locationList.get(i).getLat() + "\nLongitude : " + locationList.get(i).getLog();
        myViewHolder.textView.setText(textToDisplay);

        File imgFile = new File(locationList.get(i).getImageUri().replace("file:", ""));
        if (imgFile.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            myViewHolder.imageView.setImageBitmap(myBitmap);
        }
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListenerCallBack.clickListenerCallBack(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return locationList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textView;
        public ImageView imageView;

        public MyViewHolder(View v) {
            super(v);
            imageView = v.findViewById(R.id.imageView);
            textView = v.findViewById(R.id.textView);
        }
    }

}
