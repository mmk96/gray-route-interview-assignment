package assignment.mayuri.interviewtest;

public class locationListModel {

    private double lat;
    private double log;
    private String address;
    private String imageUri;

    public locationListModel(double lat, double log, String address, String imageUri) {
        this.lat = lat;
        this.log = log;
        this.address = address;
        this.imageUri = imageUri;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public double getLog() {
        return log;
    }

    public void setLog(double log) {
        this.log = log;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
